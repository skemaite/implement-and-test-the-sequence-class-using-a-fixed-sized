// FILE: sequence1.cxx
// CLASS IMPLEMENTED: sequence (see sequence1.h for documentation)
//1. The number of items in the sequence is stored in the member variable
//used.
//2. For an empty sequence, we do not care what is stored in any of data; for
//a non-empty sequence, the items are stored in their sequence order from
//data[0] to data[used-1], and we don’t care what is stored in the rest of
//data.
//3. If there is a current item, then it lies in data[current_index]; if there is
//no current item, then current_index equals used.

#include <iostream>
#include <cstdlib>
#include <cassert> // Provides assert function
#include "sequence1.h"

namespace main_savitch_3
{
	sequence::sequence ( )
	{
		used = 0;
		current_index = 0;
	}

	void sequence::start ( )
	{
        current_index = 0;
	}

        sequence::size_type sequence::size( ) const
        {
        		return used;
        }

        bool sequence::is_item( ) const
        {
            return used != 0 && current_index >= 0 && current_index <= used-1;
        }


        sequence::value_type sequence::current( ) const
        {
        	if(is_item())
                return data[current_index];
        }

    void sequence::advance( )
    {
    	if(is_item()) {
            if (current_index == used - 1)
                current_index = used;
            else
                current_index++;
        }
    }


    void sequence::insert(const value_type& entry)
    {
    	if(size( ) < CAPACITY) {
            size_type i;
            if (current_index >= used)
              current_index = 0;
            for (i = used; i > current_index; --i)
                data[i] = data[i-1];
            data[current_index] = entry;
            used++;
        }
    }

    void sequence::attach(const value_type& entry)
    {
    	if(size( ) < CAPACITY) {
            if (used == 0) {
                data[current_index] = entry;
            } else if(current_index >= used){
                current_index = used;
                data[current_index] = entry;
            }else{
                for (size_t j = used; j > current_index + 1; --j)
                    data[j] = data[j-1];

                data[current_index+1] = entry;
                current_index++;
            }
            used++;
        }

    }

    void sequence::remove_current( )
    {
    	if(!is_item())
            return;
        if (current_index == used - 1)
        {
            current_index = used - 1;
            used--;
        }
        else {
            for (size_type i = current_index; i < used; i++)
                data[i] = data[i+1];
             used--;
            }
            
    	}
        


}
